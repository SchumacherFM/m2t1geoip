<?php

namespace SchumacherFM\M2T1Geoip\Model;

use Magento\Framework\HTTP\ClientInterface;
use Psr\Log\LoggerInterface as Logger;

class Geoip implements \SchumacherFM\M2T1Geoip\Api\GeoipInterface
{
    const NIRVANA = 'XX';

    const URL = 'http://freegeoip.net/json/';

    /**
     * @var ClientInterface
     */
    private $client;

    /**
     * @var \Magento\Framework\HTTP\PhpEnvironment\RemoteAddress
     */
    private $remoteAddress;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @todo \Magento\Framework\Json\DecoderInterface $jsonDecoder
     * Geoip constructor.
     *
     * @param ClientInterface                                      $client
     * @param Logger                                               $logger
     * @param \Magento\Framework\HTTP\PhpEnvironment\RemoteAddress $remoteAddress
     */
    public function __construct(ClientInterface $client, Logger $logger, \Magento\Framework\HTTP\PhpEnvironment\RemoteAddress $remoteAddress)
    {
        $this->client = $client;
        $this->logger = $logger;
        $this->remoteAddress = $remoteAddress;
    }

    /**
     * {@inheritdoc}
     */
    public function getCountryCode()
    {
        $result = $this->doRequest();
        if (is_array($result) && isset($result['country_code'])) {
            return $result['country_code'];
        }
        return self::NIRVANA;
    }

    /**
     * array (size=11)
     * 'ip' => string '193.17.194.226' (length=14)
     * 'country_code' => string 'CH' (length=2)
     * 'country_name' => string 'Switzerland' (length=11)
     * 'region_code' => string 'ZH' (length=2)
     * 'region_name' => string 'Zurich' (length=6)
     * 'city' => string 'Zurich' (length=6)
     * 'zip_code' => string '8010' (length=4)
     * 'time_zone' => string 'Europe/Zurich' (length=13)
     * 'latitude' => float 47.367
     * 'longitude' => float 8.55
     * 'metro_code' => int 0
     *
     * @return bool|array
     */
    protected function doRequest()
    {
        try {
            $this->client->get(self::URL . $this->remoteAddress->getRemoteAddress());
//            $this->client->get('http://freegeoip.net/json/193.17.194.226');
            $status = $this->client->getStatus();
            $result = $this->client->getBody();

        } catch (\Exception $e) {
            $this->logger->warning($e->getMessage());
            $this->logger->warning($e->getTraceAsString());
            // do nothing if remote isn't available
            return false;
        }

        if (200 !== $status) {
            $this->logger->warning('Request Status is not 200');
            return false;
        }

        return json_decode($result, true);
    }

}

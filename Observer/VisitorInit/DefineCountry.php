<?php
/**
 * Copyright © 2015 Cyrill Schumacher. All rights reserved.
 */
namespace SchumacherFM\M2T1Geoip\Observer\VisitorInit;

use Magento\Framework\Event\ObserverInterface;
use SchumacherFM\M2T1Geoip\Api\GeoipInterface;

class DefineCountry implements ObserverInterface
{
    /**
     * @var \Magento\Customer\Model\Visitor
     */
    private $_customerVisitor;

    /**
     * @var GeoipInterface
     */
    private $_geopi;

    public function __construct(GeoipInterface $geoip)
    {
        $this->_geopi = $geoip;
    }

    /**
     * Visitor_Init event calls this observer and it sets the country code
     * to the customer visitor object.
     *
     * @param \Magento\Framework\Event\Observer $observer
     *
     * @return $this
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $this->_customerVisitor = $observer->getData('visitor');

        $this->_customerVisitor->setData('country_code', $this->_geopi->getCountryCode());
        // do what ever you want with the country code

        return $this;
    }
}

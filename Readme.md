## Magento 2 Trained Partner Program: Developer Exercises 

# Unit 1 FreeGeoIP

Create an extension that defines the country of every visitor by making a remote call to the freegeoip.net/json/_IP_. (Training1_FreeGeoIp)

<?php

namespace SchumacherFM\M2T1Geoip\Test\Unit\Model;

class GeoipTest extends \PHPUnit_Framework_TestCase
{

    /** @var \Magento\Framework\HTTP\PhpEnvironment\RemoteAddress|\PHPUnit_Framework_MockObject_MockObject */
    private $remoteAddressMock;

    /** @var \SchumacherFM\M2T1Geoip\Model\Geoip|\PHPUnit_Framework_MockObject_MockObject */
    private $_model = null;

    /** @var \Magento\Framework\HTTP\ClientInterface|\PHPUnit_Framework_MockObject_MockObject */
    private $_client = null;

    protected function setUp()
    {
        parent::setUp();

        $this->_client = $this->getMock('Magento\Framework\HTTP\ClientInterface');
        $logger = $this->getMockForAbstractClass('Psr\Log\LoggerInterface', [], '', false);


        $this->remoteAddressMock = $this->getMock(
            '\Magento\Framework\HTTP\PhpEnvironment\RemoteAddress',
            ['getRemoteAddress'],
            [],
            '',
            false
        );

        $helper = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);
        $this->_model = $helper->getObject(
            'SchumacherFM\M2T1Geoip\Model\Geoip',
            [
                'client'        => $this->_client,
                'logger'        => $logger,
                'remoteAddress' => $this->remoteAddressMock,
            ]
        );
        $this->remoteAddressMock->expects($this->once())
            ->method('getRemoteAddress')
            ->will(
                $this->returnValue('127.0.0.1')
            );
        $this->_client
            ->expects($this->once())
            ->method('get')
            ->with(\SchumacherFM\M2T1Geoip\Model\Geoip::URL . '127.0.0.1')
            ->will($this->returnSelf());

        $this->_client
            ->expects($this->once())
            ->method('getBody')
            ->with()
            ->will(
                $this->returnValue(
                    '{"ip":"127.0.0.1","country_code":"GO","country_name":"","region_code":"","region_name":"","city":"","zip_code":"","time_zone":"","latitude":0,"longitude":0,"metro_code":0}'
                )
            );
        $this->_client
            ->expects($this->once())
            ->method('getStatus')
            ->with()
            ->will($this->returnValue(200));
    }

    public function testgetCountryCode()
    {
        $this->assertEquals('GO', $this->_model->getCountryCode());
    }
}